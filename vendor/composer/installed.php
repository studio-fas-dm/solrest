<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'drupal-module',
        'install_path' => __DIR__ . '/../solrest/',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'drupal/solrest',
        'dev' => true,
    ),
    'versions' => array(
        'drupal/solrest' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'drupal-module',
            'install_path' => __DIR__ . '/../solrest/',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
    ),
);
