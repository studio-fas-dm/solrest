<?php

namespace Drupal\solrest\Plugin\Deriver;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Solrest resource plugin definition for every solr search api index.
 *
 * @see \Drupal\solrest\Plugin\rest\resource\SolrestResource
 */
class SolrDeriver implements ContainerDeriverInterface {

  /**
   * List of derivative definitions.
   *
   * @var array
   */
  protected $derivatives;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs an EntityDeriver object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinition($derivative_id, $base_plugin_definition) {
    if (!isset($this->derivatives)) {
      $this->getDerivativeDefinitions($base_plugin_definition);
    }
    if (isset($this->derivatives[$derivative_id])) {
      return $this->derivatives[$derivative_id];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    if (!isset($this->derivatives)) {
      /** @var \Drupal\search_api\Entity\Server[] $servers */
      $servers = \Drupal::entityTypeManager()->getStorage('search_api_server')->loadMultiple();

      foreach ($servers as $server_id => $server) {

        $server_label = $server->label();

        if ($server && $server->getBackend()->getPluginId() == 'search_api_solr') {
          $server_identifier = 'solrest:server' . $server_id;
          $this->derivatives[$server_identifier] = [
            'id' => $server_identifier,
            'server_id' => $server_id,
            'label' => "Solrest server $server_label",
            'uri_paths' => [
              'canonical' => "/solrest/" . '{' . $server_id . '}',
            ],
          ];
          $this->derivatives[$server_identifier] += $base_plugin_definition;
        }
      }
    }

    return $this->derivatives;
  }

}
